﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex1
{
    public partial class Dates : Form
    {
        public string username{get; set;}
        string celebrator, celebratorDate;

        public Dates()
        {
            InitializeComponent();
        }

        private void Dates_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string date = null, name;
            int year, pos;
            celebratorDate = monthCalendar1.SelectionEnd.ToShortDateString();
            year = celebratorDate.LastIndexOf('/');
            celebratorDate = celebratorDate.Substring(0, year); //check month and day, not year

            if (File.Exists(username + "BD.txt")) //if file doesnt exist, create new one
            {
                using (StreamReader sr = new StreamReader(username + "BD.txt"))
                {
                    //continue to read until end of file
                    while (!sr.EndOfStream)
                    {
                        // Read the stream to a string, and write the string to the console.
                        String line = sr.ReadLine();
                        if(line == "")
                        {
                            break;
                        }
                        pos = line.IndexOf(',');
                        name = line.Substring(0, pos);
                        date = line.Substring(pos + 1);
                        year = date.LastIndexOf('/');
                        date = date.Substring(0, year); //check month and day, not year
                        if (date.CompareTo(celebratorDate) == 0)
                        {
                            this.textBox1.Text = "In the chosen date - " + name + " celebrates birthday!";
                            break;
                        }
                    }

                    if(date.CompareTo(celebratorDate) != 0)
                    {
                        this.textBox1.Text = "In the chosen date - no one celebrates birthday!";
                    }
                }
            }
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name, date = null;
            int pos;
            if (celebrator != null)
            {
                using (StreamReader sr = new StreamReader(username + "BD.txt"))
                {
                    //continue to read until end of file
                    while (!sr.EndOfStream)
                    {
                        // Read the stream to a string, and write the string to the console.
                        String line = sr.ReadLine();
                        if (line == "")
                        {
                            break;
                        }
                        pos = line.IndexOf(',');
                        name = line.Substring(0, pos);
                        date = line.Substring(pos + 1);
                        if (date.CompareTo(dateTimePicker1.Value.ToShortDateString()) == 0)
                        {
                            MessageBox.Show(name + " already celebrates on this date!");
                            break;
                        }
                    }
                    sr.Close();
                    if(date.CompareTo(dateTimePicker1.Value.ToShortDateString()) != 0)
                    {
                        if (File.Exists(username + "BD.txt")) //if file doesnt exist, create new one
                        {
                            using (StreamWriter sw = File.AppendText(username + "BD.txt"))
                            {
                                sw.WriteLine(celebrator + "," + dateTimePicker1.Value.ToShortDateString());
                            }
                        }
                        else
                        {
                            File.Open(username + "BD.txt", FileMode.CreateNew); //creates new file
                            using (StreamWriter sw = File.CreateText(username + "BD.txt"))
                            {
                                sw.WriteLine(celebrator + "," + dateTimePicker1.Value.ToShortDateString());
                            }
                        }
                    }
                   
                }
                dateTimePicker1.ResetText();
                textBox3.ResetText();
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e) //Add celebrator textbox
        {
            TextBox t = (TextBox)sender;
            celebrator = t.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e) //Birthday anouncer textbox
        {

        }
    }
}
