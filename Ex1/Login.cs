﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex1
{
    public partial class Login : Form
    {
        string username, password;
        public Login()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e) //username texbox
        {
            TextBox textBox = (TextBox)sender;
            username = textBox.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e) //password textbox
        {
            TextBox textBox = (TextBox)sender;
            password = textBox.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        { 
            if(password != null && username != null)
            {
                // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader("Users.txt"))
                {
                    //continue to read until end of file
                    while(!sr.EndOfStream)
                    {
                        // Read the stream to a string, and write the string to the console.
                        String line = sr.ReadLine();
                        int pos = line.IndexOf(',');
                        string name = line.Substring(0, pos);
                        string pass = line.Substring(pos + 1);
                        if (username.CompareTo(name) == 0 && password.CompareTo(pass) == 0)
                        {
                            this.Visible = false;
                            Dates f2 = new Dates();
                            f2.username = username;
                            f2.ShowDialog();
                        }
                    }
                }
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
